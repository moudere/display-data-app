import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FormGroup } from '@angular/forms';
//import { AppRoutingModule } from './app-routing.module';
//import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
//import { HomeComponent } from './home/home.component';
import { AppComponent} from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
//    HomeComponent,
 //   FootballerComponent
  ],
  imports: [
    BrowserModule,
 //   AppRoutingModule,
    NgbModule,
    HttpClientModule,
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
  //  FormGroup
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
