import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormsModule} from '@angular/forms';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { FormGroup } from '@angular/forms';

import { FormBuilder } from '@angular/forms';

export class Footballer {
  constructor(
    public id: string,
    public name: string,
    public club: string,
  ) {
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  footballers: Footballer[];
  httpClient: HttpClient;
  closeResult: String;
  private modalService: NgbModal;
  editForm: FormGroup;
  fb: FormBuilder;
  deleteId: string;
  constructor(httpClient: HttpClient,private ngbModal: NgbModal, fb: FormBuilder) { 
	this.footballers=[];
	this.httpClient=httpClient;
	this.closeResult="" ;
	this.modalService=ngbModal;
	this.fb=fb;
	this.editForm = this.fb.group({})
	this.deleteId="";
}
getFootballers(){
    
    this.httpClient.get<any>( 'https://xg2846toj2.execute-api.us-east-2.amazonaws.com/footballers').subscribe(
	response =>{
	console.log(response.Items);
	this.footballers=response.Items;
	
}
);
}
open(content: any) {
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return `with: ${reason}`;
  }
}
onSubmit(f: any) {
  const url = 'https://xg2846toj2.execute-api.us-east-2.amazonaws.com/footballers';
  this.httpClient.put(url, f.value)
    .subscribe((result) => {
      this.ngOnInit(); //reload the table
    });
  this.modalService.dismissAll(); //dismiss the modal
}

openEdit(targetModal: any, footballer: Footballer) {
  this.modalService.open(targetModal, {
    backdrop: 'static',
    size: 'lg'
  });
  this.editForm.patchValue( {
    id: footballer.id, 
    name: footballer.name,
    club: footballer.club,
   
  });
}

onSave() {
  const editURL = 'https://xg2846toj2.execute-api.us-east-2.amazonaws.com/footballers'  ;
  console.log(this.editForm.value);
  this.httpClient.put(editURL, this.editForm.value)
    .subscribe((results) => {
      this.ngOnInit();
	});
      this.modalService.dismissAll();
   
}
openDelete(targetModal: any, footballer: Footballer) {
  this.deleteId = footballer.id;
  this.modalService.open(targetModal, {
    backdrop: 'static',
    size: 'lg'
  });
}
onDelete() {
  const deleteURL = 'https://xg2846toj2.execute-api.us-east-2.amazonaws.com/footballers/' + this.deleteId;
  this.httpClient.delete(deleteURL)
    .subscribe((results) => {
      this.ngOnInit();
      this.modalService.dismissAll();
    });
}
  ngOnInit(): void {
	
	this.getFootballers() ;
	this.editForm = this.fb.group({
    id: [''],
    name: [''],
    club: [''],

  } );
  }

}

